import {fromJS} from "immutable";

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const userInitialState = fromJS({
	logged: null,
});

/**
 * Merge route into the global application state
 */
export default function routeReducer(state = userInitialState, action) {
	switch (action.type) {
		default:
			return state;
	}
}