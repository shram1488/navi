import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Container, Text, Button, Content} from 'native-base';
import {createStackNavigator} from 'react-navigation';
import Expo from "expo";


export class Auth extends React.Component {
	static navigationOptions = {
		title: 'Auth',
		headerStyle: {
			display: 'none',
		},
	};
	
	render() {
		return (
			<Container style={styles.container}>
				<Image
					style={styles.logo}
					source={require('./../../media/img/logo.jpg')}
				/>
				<Content style={styles.button}>
					<Button
						bordered
						success
						onPress={() => this.props.navigation.navigate('Login')}
					>
						<Text
							style={styles.buttonText}
						>Войти по номеру телефона</Text>
					</Button>
				</Content>
				<Content style={styles.text}>
					<Text>
						Осуществляя вход, вы выражаете согласие, с нашими условиями обслуживания и политикой конфедециальности.
					</Text>
				</Content>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	buttonText: {
		color: '#000',
	},
	logo: {
		width: 100,
		height: 100,
		position: 'absolute',
		top: 50,
	},
	button: {
		marginTop: 250,
	},
	text: {
		marginTop: 50,
		width: '80%',
	}
});


export default Auth;