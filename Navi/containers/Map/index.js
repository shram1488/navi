import React from 'react';
import {
	StyleSheet,
	View,
	Text,
	Dimensions,
	TouchableOpacity,
	Platform,
} from 'react-native';

import Image from 'react-native-remote-svg'

import MapView, { ProviderPropType, Marker, AnimatedRegion } from 'react-native-maps';
import {Container, Button, Text as Text2} from 'native-base';
import { createStackNavigator } from 'react-navigation';
import Expo from "expo";


const screen = Dimensions.get('window');

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


export class Map extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			coordinate: new AnimatedRegion({
				latitude: LATITUDE,
				longitude: LONGITUDE,
			}),
			traffic: false,
			lastLat: LATITUDE,
			lastLong: LONGITUDE,
			latitudeDelta:  LATITUDE_DELTA,
			longitudeDelta: LONGITUDE_DELTA,
			statusBarHeight: 0,
		};
	}
	
	animate() {
		const { coordinate } = this.state;
		const newCoordinate = {
			latitude: LATITUDE + ((Math.random() - 0.5) * (LATITUDE_DELTA / 2)),
			longitude: LONGITUDE + ((Math.random() - 0.5) * (LONGITUDE_DELTA / 2)),
		};
		
		if (Platform.OS === 'android') {
			if (this.marker) {
				this.marker._component.animateMarkerToCoordinate(newCoordinate, 500);
			}
		} else {
			coordinate.timing(newCoordinate).start();
		}
	}
	
	test = () => {
		this.setState({
			traffic : !this.state.traffic,
		})
	}
	
	
	
	zoomIn = () => {
		let region = {
			latitude:       this.state.lastLat,
			longitude:      this.state.lastLong,
			latitudeDelta:        this.state.latitudeDelta / 10,
			longitudeDelta:        this.state.longitudeDelta / 10
		}
		this.state.latitudeDelta = region.latitudeDelta;
		this.state.longitudeDelta = region.longitudeDelta;
		this.state.lastLat = region.latitude;
		this.state.lastLong = region.longitude;
		this.map.animateToRegion(region, 300);
	}
	
	zoomOut = () => {
		let region = {
			latitude:       this.state.lastLat,
			longitude:      this.state.lastLong,
			latitudeDelta:        this.state.latitudeDelta * 10,
			longitudeDelta:        this.state.longitudeDelta * 10
		}
		this.state.latitudeDelta = region.latitudeDelta;
		this.state.longitudeDelta = region.longitudeDelta;
		this.state.lastLat = region.latitude;
		this.state.lastLong = region.longitude;
			
			this.map.animateToRegion(region, 300);
	}
	
	render() {
		return (
			<View style={styles.container}>
				<MapView
					ref={ref => { this.map = ref; }}
					provider={this.props.provider}
					style={styles.map}
					initialRegion={{
						latitude: this.state.lastLat,
						longitude: this.state.lastLong,
						latitudeDelta: this.state.latitudeDelta,
						longitudeDelta: this.state.longitudeDelta,
					}}
					showsCompass
					showsTraffic={this.state.traffic}
					showsBuildings
				>
					<Marker.Animated
						ref={marker => { this.marker = marker; }}
						coordinate={this.state.coordinate}
					/>
				</MapView>
				<TouchableOpacity style={styles.search}>
					<Image source={require('./../../media/svg/search_map.svg')} style={{width: '100%', height: '100%'}}/>
				</TouchableOpacity>
				<TouchableOpacity style={styles.traffic} onPress={this.test}>
					<Image source={require('./../../media/svg/traffic_off.svg')} style={{width: '100%', height: '100%'}}/>
				</TouchableOpacity>
				<TouchableOpacity style={styles.menu} onPress={this.test}>
					<Image source={require('./../../media/svg/menu.svg')} style={{width: '100%', height: '100%'}}/>
				</TouchableOpacity>
				<TouchableOpacity style={styles.voice} onPress={this.test}>
					<Image source={require('./../../media/svg/voice.svg')} style={{width: '100%', height: '100%'}}/>
				</TouchableOpacity>
				<TouchableOpacity style={styles.zoomIn} onPress={this.zoomIn}>
					<Image source={require('./../../media/svg/zoomIn.svg')} style={{width: '100%', height: '100%'}}/>
				</TouchableOpacity>
				<TouchableOpacity style={styles.zoomOut} onPress={this.zoomOut}>
					<Image source={require('./../../media/svg/zoomOut.svg')} style={{width: '100%', height: '100%'}}/>
				</TouchableOpacity>
				<TouchableOpacity style={styles.location} onPress={this.test}>
					<Image source={require('./../../media/svg/location.svg')} style={{width: '100%', height: '100%'}}/>
				</TouchableOpacity>
			</View>
		);
	}
}

Map.propTypes = {
	provider: ProviderPropType,
};

const styles = StyleSheet.create({
	container: {
		...StyleSheet.absoluteFillObject,
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	map: {
		...StyleSheet.absoluteFillObject,
	},
	search: {
		position: 'absolute',
		width: 70,
		height: 70,
		bottom: 5,
		left: 100,
	},
	traffic: {
		position: 'absolute',
		width: 60,
		height: 60,
		top: 40,
		left: 10,
		padding: 0,
	},
	voice: {
		position: 'absolute',
		width: 70,
		height: 70,
		bottom: 5,
		left: 10,
		padding: 0,
	},
	menu: {
		position: 'absolute',
		width: 70,
		height: 70,
		bottom: 5,
		right: 10,
		padding: 0,
	},
	zoomIn: {
		position: 'absolute',
		width: 70,
		height: 70,
		top: 200,
		right: 10,
		padding: 0,
	},
	zoomOut: {
		position: 'absolute',
		width: 70,
		height: 70,
		top: 270,
		right: 10,
		padding: 0,
	},
	location: {
		position: 'absolute',
		width: 80,
		height: 80,
		top: 340,
		right: 5,
		padding: 0,
	},
	full: {
		width: '100%',
		height: '100%',
	}
});

export default Map;