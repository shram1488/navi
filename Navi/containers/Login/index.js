import React from 'react';
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import {Container, Text, Button, Content, Header, Body, Title} from 'native-base';
import {createStackNavigator} from 'react-navigation';
import Expo from "expo";
import PhoneInput from 'react-native-phone-input'

import ModalPickerImage from './ModalPickerImage';


export class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			valid: "",
			type: "",
			value: "+7"
		};
	}
	
	static navigationOptions = {
		title: 'Login',
	};
	
	updateInfo = () => {
		this.setState({
			valid: this.phone.isValidNumber(),
			type: this.phone.getNumberType(),
			value: this.phone.getValue()
		});
	}
	
	renderInfo = () => {
		if (this.state.value) {
			return (
				<View style={styles.info}>
					<Text>
						Is Valid:{" "}
						<Text style={{fontWeight: "bold"}}>
							{this.state.valid.toString()}
						</Text>
					</Text>
					<Text>
						Type: <Text style={{fontWeight: "bold"}}>{this.state.type}</Text>
					</Text>
					<Text>
						Value:{" "}
						<Text style={{fontWeight: "bold"}}>{this.state.value}</Text>
					</Text>
				</View>
			);
		}
	}
	
	
	render() {
		return (
			<Container style={styles.container}>
				<Content>
					<PhoneInput
						style={styles.input}
						ref={ref => {
							this.phone = ref;
						}}
					/>
					
					<TouchableOpacity
						onPress={() => this.props.navigation.navigate('Map')}
						style={styles.button}>
						<Text>Далее</Text>
					</TouchableOpacity>
				</Content>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	buttonText: {
		color: '#000',
	},
	logo: {
		width: 100,
		height: 100,
		position: 'absolute',
		top: 50,
	},
	button: {
		marginTop: 250,
	},
	text: {
		marginTop: 50,
	},
	info: {
		// width: 200,
		borderRadius: 5,
		backgroundColor: "#f0f0f0",
		padding: 10,
		marginTop: 20
	},
	button: {
		marginTop: 20,
		padding: 10
	},
	input: {
		marginTop: 200,
	}
});


export default Login;