import React from 'react';
import {Provider} from 'react-redux';

import {StyleSheet, Text, View, Image} from 'react-native';
import {Container, Button, Text as Text2} from 'native-base';
import {createStackNavigator} from 'react-navigation';
import Expo from "expo";

import configureStore from './configureStore';

import Auth from './containers/Auth';
import Map from './containers/Map';
import Login from './containers/Login';


const store = configureStore({});
export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {loading: true};
	}
	
	async componentWillMount() {
		await Expo.Font.loadAsync({
			'Roboto': require('native-base/Fonts/Roboto.ttf'),
			'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
			'Ionicons': require('@expo/vector-icons/fonts/Ionicons.ttf'),
		});
		this.setState({loading: false});
		console.log('lol');
	}
	
	render() {
		const resizeMode = 'center';
		if (this.state.loading) {
			return <Image
				style={{
					flex: 1,
					width: '100%',
					height: '100%'
				}}
				source={require('./media/img/splash.png')}
			/>;
		}
		return (
			<Provider store={store}>
				<RootStack/>
			</Provider>
		);
	}
}

const RootStack = createStackNavigator(
	{
		Map: Map,
		Auth: Auth,
		Login: Login,
	},
	{
		initialRouteName: 'Auth',
	}
);

const
	styles = StyleSheet.create({
		container: {
			flex: 1,
			alignItems: 'center',
			justifyContent: 'center',
		},
	});
	
	
