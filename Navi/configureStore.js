/**
 * Create the store with dynamic reducers
 */

import {combineReducers} from "redux-immutable";
import {createStore, applyMiddleware, compose} from "redux";
import {fromJS} from "immutable";
import createSagaMiddleware from "redux-saga";
import userReducer from "./containers/Auth/reducer";

const sagaMiddleware = createSagaMiddleware();

/**
 * Creates the main reducer with the dynamically injected ones
 */
export function createReducer(injectedReducers) {
	return combineReducers({
		user: userReducer,
		...injectedReducers,
	});
}

export default function configureStore(reducers = {},
																			 initialState = {},
																			 middlewares = [],) {
	// Create the store with 3 middlewares
	// 1. sagaMiddleware: Makes redux-sagas work
	// 2. thunkMiddleware: Makes redux-thunk work
	// 3. routerMiddleware: Syncs the location/URL path to the state
	const middlewaresToApply = [
		sagaMiddleware,
		...middlewares,
	];
	
	const enhancers = [applyMiddleware(...middlewaresToApply)];
	
	// If Redux DevTools Extension is installed use it, otherwise use Redux compose
	/* eslint-disable no-underscore-dangle */
	const composeEnhancers =
		typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
			? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
				// TODO Try to remove when `react-router-redux` is out of beta, LOCATION_CHANGE should not be fired more than once after hot reloading
				// Prevent recomputing reducers for `replaceReducer`
				shouldHotReload: false,
			})
			: compose;
	/* eslint-enable */
	
	const store = createStore(
		createReducer(reducers),
		fromJS(initialState),
		composeEnhancers(...enhancers),
	);
	
	// Extensions
	store.runSaga = sagaMiddleware.run;
	store.injectedReducers = reducers; // Reducer registry
	store.injectedSagas = {}; // Saga registry
	
	
	return store;
}